package com.example.vito.pizza.service;

import com.example.vito.pizza.modal.dto.CartDTO;
import com.example.vito.pizza.modal.entity.Cart;

public interface CartService {

    CartDTO getCartDetailsByUserId(Long userId);

    CartDTO addCartDetails(CartDTO cartDto);

    CartDTO updateCartDetails(CartDTO cartDto);
}
