package com.example.vito.pizza.service.Impl;

import com.example.vito.pizza.modal.dto.OrderDTO;
import com.example.vito.pizza.modal.entity.Order;
import com.example.vito.pizza.repository.OrderRepository;
import com.example.vito.pizza.service.OrderService;
import com.example.vito.pizza.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Override
    public List<OrderDTO> getAllOrders() {
        List<Order> orders = orderRepository.findAll();
        if(orders.isEmpty()){
            return null;
        }
        return orders.stream().map(Mapper::mapOrderEntityToOrderDTO).collect(Collectors.toList());
    }

    @Override
    public List<OrderDTO> getOrdersByCategory(String orderType) {
        List<Order> orders = orderRepository.findByOrderType(orderType);
        if(orders.isEmpty()){
            return null;
        }
        return orders.stream().map(Mapper::mapOrderEntityToOrderDTO).collect(Collectors.toList());
    }

}
