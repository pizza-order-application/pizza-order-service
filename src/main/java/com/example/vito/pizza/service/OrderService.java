package com.example.vito.pizza.service;

import com.example.vito.pizza.modal.dto.OrderDTO;

import java.util.List;

public interface OrderService {

    List<OrderDTO> getAllOrders();

    List<OrderDTO> getOrdersByCategory(String orderType);
}
