package com.example.vito.pizza.service.Impl;

import com.example.vito.pizza.modal.dto.InvoiceDTO;
import com.example.vito.pizza.modal.dto.PaymentDTO;
import com.example.vito.pizza.modal.entity.Invoice;
import com.example.vito.pizza.repository.InvoiceRepository;
import com.example.vito.pizza.service.InvoiceService;
import com.example.vito.pizza.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceServiceImpl implements InvoiceService {
    private static final Logger logger = LoggerFactory.getLogger(InvoiceServiceImpl.class);

    @Autowired
    InvoiceRepository invoiceRepository;

    @Override
    public InvoiceDTO getInvoiceByInvoiceNo(String invoiceNo) {
        if(invoiceNo == null){
            return null;
        }
        Invoice invoice = invoiceRepository.findByInvoiceNo(invoiceNo);
        if(invoice == null){
            return null;
        }
        return Mapper.mapInvoiceToInvoiceDTo(invoice);
    }

    @Override
    public InvoiceDTO printInvoice(InvoiceDTO invoiceDTO) {
        if(invoiceDTO == null){
            return null;
        }
        if(!this.validateAmount(invoiceDTO)){
            return null;
        }
        Invoice saveInvoice = invoiceRepository.saveAndFlush(Mapper.mapInvoiceDTOToInvoiceEntity(invoiceDTO));
        return Mapper.mapInvoiceToInvoiceDTo(saveInvoice);
    }

    private boolean validateAmount(InvoiceDTO invoiceDTO) {
        double paymentAmount = invoiceDTO.getPaymentList().stream().mapToDouble(PaymentDTO::getAmount).sum();
        if (paymentAmount != invoiceDTO.getInvoiceTotal()) {
            logger.info("Miss Match Payment | Total Payment != Invoice Total {}{}", paymentAmount, invoiceDTO.getInvoiceTotal());
            return false;
        }
        return true;
    }
}
