package com.example.vito.pizza.service;

import com.example.vito.pizza.modal.dto.InvoiceDTO;

public interface InvoiceService {

    InvoiceDTO getInvoiceByInvoiceNo(String invoiceNo);

    InvoiceDTO printInvoice(InvoiceDTO invoiceDTO);
}
