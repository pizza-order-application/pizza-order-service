package com.example.vito.pizza.service.Impl;

import com.example.vito.pizza.modal.dto.UserDTO;
import com.example.vito.pizza.modal.entity.User;
import com.example.vito.pizza.repository.UserRepository;
import com.example.vito.pizza.service.UserService;
import com.example.vito.pizza.util.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDTO getUserById(Long userId) {
        if(userId == null){
            return null;
        }
        User user = userRepository.findByUserIdAndIsDeleted(userId, false);
        if(user == null) return null;
        return Mapper.mapUserToUserDTO(user);
    }

    @Override
    public UserDTO registerUser(UserDTO userDTO) {
        if(userDTO == null) return null;
        if(userRepository.findByUserIdAndIsDeleted(userDTO.getUserId(),false) != null){
            logger.info("User Already Registered {}{}",userDTO.getUserId(),userDTO.getUserName());
            return null;
        }
        User saveUser = userRepository.saveAndFlush(Mapper.mapUserDtoToUser(userDTO));
        return Mapper.mapUserToUserDTO(saveUser);
    }

    @Override
    public UserDTO loginUser(UserDTO userDTO) {
        if(userDTO == null) return null;
        User activeSessionUser = userRepository.findByUserIdAndIsDeletedAndIsActiveSession(userDTO.getUserId(), false, true);
        if(activeSessionUser != null){
            logger.info("User Already Login {}{}",userDTO.getUserId(),userDTO.getUserName());
            return null;
        }
        if(userRepository.findByUserIdAndIsDeletedAndIsActiveSessionAndPassword(userDTO.getUserId(), false, false, userDTO.getPassword()) == null){
            logger.info("Password Mismatch {}{}",userDTO.getUserId(),userDTO.getUserName());
            return null;
        }
        User user = Mapper.mapUserDtoToUser(userDTO);
        user.setIsActiveSession(true);
        userRepository.saveAndFlush(user);
        return Mapper.mapUserToUserDTO(user);
    }
}
