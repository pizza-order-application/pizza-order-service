package com.example.vito.pizza.service.Impl;

import com.example.vito.pizza.modal.dto.CartDTO;
import com.example.vito.pizza.modal.entity.Cart;
import com.example.vito.pizza.modal.entity.CartOrder;
import com.example.vito.pizza.repository.CartRepository;
import com.example.vito.pizza.service.CartService;
import com.example.vito.pizza.util.Mapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    CartRepository cartRepository;

    @Override
    public CartDTO getCartDetailsByUserId(Long userId) {
        if(userId == null){
            return null;
        }
        Cart cart = cartRepository.findByUserIdAndIsDeleted(userId, false);
        if(cart == null){
            return null;
        }
        CartDTO cartDTO =  new CartDTO();
        BeanUtils.copyProperties(cart,cartDTO);
        return cartDTO;
    }

    @Override
    public CartDTO addCartDetails(CartDTO cartDto) {
        if(cartDto == null){
            return null;
        }
        Cart cart = new Cart();
        BeanUtils.copyProperties(cartDto,cart);
        List<CartOrder> cartOrders =  new ArrayList<>();
        cartDto.getCartOrderDTOS().forEach(o -> cartOrders.add(Mapper.mapCartOrderDtoToEntity(o)));
        cart.setOrder(cartOrders);

        Cart saveCart = cartRepository.saveAndFlush(cart);
        return Mapper.mapCartToCartDTO(saveCart);
    }

    @Override
    public CartDTO updateCartDetails(CartDTO cartDto) {
        if(cartDto.getId() == null){
            return null;
        }
        Cart cart = cartRepository.findById(cartDto.getId()).orElse(null);
        if(cart == null){
            return null;
        }

        List<CartOrder> cartOrders =  new ArrayList<>();
        cartDto.getCartOrderDTOS().forEach(o -> cartOrders.add(Mapper.mapCartOrderDtoToEntity(o)));
        cart.setOrder(cartOrders);
        cart.setTotalPrice(cartDto.getTotalPrice());
        Cart updateCart = cartRepository.saveAndFlush(cart);
        return Mapper.mapCartToCartDTO(updateCart);
    }
}
