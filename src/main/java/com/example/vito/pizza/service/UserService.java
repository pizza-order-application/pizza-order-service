package com.example.vito.pizza.service;

import com.example.vito.pizza.modal.dto.UserDTO;

public interface UserService {
    UserDTO getUserById(Long userId);

    UserDTO registerUser(UserDTO userDTO);

    UserDTO loginUser(UserDTO userDTO);
}
