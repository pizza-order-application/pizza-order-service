package com.example.vito.pizza.util;

import com.example.vito.pizza.modal.dto.*;
import com.example.vito.pizza.modal.entity.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class Mapper {

    public static OrderDTO mapOrderEntityToOrderDTO(Order order) {
        OrderDTO orderDTO = new OrderDTO();
        BeanUtils.copyProperties(order,orderDTO);
        return orderDTO;
    }

    public static CartOrder mapCartOrderDtoToEntity(CartOrderDTO cartOrderDTO) {
        CartOrder cartOrder = new CartOrder();
        BeanUtils.copyProperties(cartOrderDTO,cartOrder);
        return cartOrder;
    }

    public static CartDTO mapCartToCartDTO(Cart saveCart) {
        CartDTO cartDTO = new CartDTO();
        BeanUtils.copyProperties(saveCart,cartDTO);
        return cartDTO;
    }

    public static InvoiceDTO mapInvoiceToInvoiceDTo(Invoice invoice) {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        List<OrderDTO> orderDTOList =  new ArrayList<>();
        List<PaymentDTO> paymentDTOList =  new ArrayList<>();

        BeanUtils.copyProperties(invoice, invoiceDTO);
        invoice.getOrderList().forEach(order -> orderDTOList.add(mapOrderEntityToOrderDTO(order)));
        invoice.getPaymentList().forEach(payment -> paymentDTOList.add(mapPaymentEntityToPaymentDTO(payment)));
        invoiceDTO.setOrderList(orderDTOList);
        invoiceDTO.setPaymentList(paymentDTOList);
        return invoiceDTO;
    }

    private static PaymentDTO mapPaymentEntityToPaymentDTO(Payment payment) {
        PaymentDTO paymentDTO = new PaymentDTO();
        BeanUtils.copyProperties(payment,paymentDTO);
        return paymentDTO;
    }

    public static Invoice mapInvoiceDTOToInvoiceEntity(InvoiceDTO invoiceDTO) {
        Invoice invoice = new Invoice();
        BeanUtils.copyProperties(invoiceDTO, invoice);

        List<Order> orders = new ArrayList<>();
        List<Payment> payments = new ArrayList<>();
        invoiceDTO.getOrderList().forEach(orderDTO -> orders.add(mapOrderDTOToOrderEntity(orderDTO)));
        invoiceDTO.getPaymentList().forEach(paymentDTO -> payments.add(mapPaymentDTOToPaymentEntity(paymentDTO)));
        invoice.setOrderList(orders);
        invoice.setPaymentList(payments);
        return invoice;
    }

    private static Payment mapPaymentDTOToPaymentEntity(PaymentDTO paymentDTO) {
        Payment payment = new Payment();
        BeanUtils.copyProperties(paymentDTO, payment);
        return payment;
    }

    private static Order mapOrderDTOToOrderEntity(OrderDTO orderDTO) {
        Order order = new Order();
        BeanUtils.copyProperties(orderDTO,order);
        return  order;
    }

    public static UserDTO mapUserToUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user, userDTO);
        return userDTO;
    }

    public static User mapUserDtoToUser(UserDTO userDTO) {
        User user = new User();
        BeanUtils.copyProperties(userDTO,user);
        return user;
    }
}
