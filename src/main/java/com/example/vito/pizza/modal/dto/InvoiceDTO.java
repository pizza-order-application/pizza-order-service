package com.example.vito.pizza.modal.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class InvoiceDTO extends BaseDTO {

    private Long id;
    private double invoiceTotal;
    private List<PaymentDTO> paymentList;
    private List<OrderDTO> orderList;
}
