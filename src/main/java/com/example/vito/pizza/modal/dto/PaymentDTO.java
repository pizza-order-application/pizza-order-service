package com.example.vito.pizza.modal.dto;

import com.example.vito.pizza.modal.enums.PaymentMethod;
import lombok.Data;

@Data
public class PaymentDTO extends BaseDTO {

    private Long id;
    private PaymentMethod paymentMethod;
    private double amount;
    private String bankName;
    private String cardNo;
}
