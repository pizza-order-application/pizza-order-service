package com.example.vito.pizza.modal.dto;

import com.example.vito.pizza.modal.entity.Order;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import lombok.Data;

import java.util.List;

@Data
public class CartDTO extends BaseDTO {

    private Long id;
    private UserDTO userDTO;
    private List<CartOrderDTO> cartOrderDTOS;
    private double totalPrice;
}
