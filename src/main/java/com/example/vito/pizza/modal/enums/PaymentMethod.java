package com.example.vito.pizza.modal.enums;

public enum PaymentMethod {

    CARD("CARD"),
    CASH("CASH");

    private String description;

    PaymentMethod(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
