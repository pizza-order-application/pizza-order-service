package com.example.vito.pizza.modal.dto;

import lombok.Data;

import java.util.Date;

@Data
public class BaseDTO {

    private Long userId;
    private String userName;
    private Date createDate;
    private Boolean isDeleted;
}
