package com.example.vito.pizza.modal.dto;

import lombok.Data;

@Data
public class UserDTO {

    private Long id;

    private String userName;
    private Long userId;
    private String idNumber;
    private String idType;
    private String password;
}
