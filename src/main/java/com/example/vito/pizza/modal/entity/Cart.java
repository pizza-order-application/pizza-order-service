package com.example.vito.pizza.modal.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Table(name = "cart_details")
@NoArgsConstructor
@AllArgsConstructor
public class Cart extends BaseEntity {

    @Id
    @Column(name = "id")
    private Long id;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "cart_id", referencedColumnName = "id")
    private List<CartOrder> cartOrders;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "cart_id", referencedColumnName = "id")
    private User user;

    private double totalPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<CartOrder> getOrder() {
        return cartOrders;
    }

    public void setOrder(List<CartOrder> order) {
        this.cartOrders = order;
    }

    public List<CartOrder> getCartOrders() {
        return cartOrders;
    }

    public void setCartOrders(List<CartOrder> cartOrders) {
        this.cartOrders = cartOrders;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
