package com.example.vito.pizza.modal.dto;

import lombok.Data;

@Data
public class CartOrderDTO extends BaseDTO {

    private Long id;
    private String orderType;
    private double price;
    private int quantity;

}
