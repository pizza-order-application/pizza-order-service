package com.example.vito.pizza.modal.dto;

import lombok.Data;

@Data
public class OrderDTO extends BaseDTO {

    private Long id;
    private String orderType;
    private double price;
    private int quantity;
    private String imageUrl;

}
