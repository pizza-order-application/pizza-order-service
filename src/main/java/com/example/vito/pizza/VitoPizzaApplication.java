package com.example.vito.pizza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VitoPizzaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VitoPizzaApplication.class, args);
	}

}
