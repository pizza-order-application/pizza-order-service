package com.example.vito.pizza.controller;

import com.example.vito.pizza.modal.dto.OrderDTO;
import com.example.vito.pizza.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @GetMapping("/get-all")
    public ResponseEntity<List<OrderDTO>> getAllOrders(){
        List<OrderDTO> orderDTOList = orderService.getAllOrders();
        if(orderDTOList.isEmpty()){
            return new ResponseEntity<>(orderDTOList, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(orderDTOList, HttpStatus.OK);
    }

    /**
     *
     * @param orderType
     * This method will return all the Pizza when we pass specific category of Pizza
     */
    @GetMapping("/get-by-category/{orderType}")
    public ResponseEntity<List<OrderDTO>> getOrdersByCategory(@PathVariable("orderType") String orderType){
        List<OrderDTO> orderDTOList = orderService.getOrdersByCategory(orderType);
        if(orderDTOList.isEmpty()){
            return new ResponseEntity<>(orderDTOList, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(orderDTOList, HttpStatus.OK);
    }

}
