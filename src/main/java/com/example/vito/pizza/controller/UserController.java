package com.example.vito.pizza.controller;

import com.example.vito.pizza.modal.dto.UserDTO;
import com.example.vito.pizza.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/get/{userId}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable("userId") Long userId){
        UserDTO userDTO = userService.getUserById(userId);
        if(userDTO == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @PostMapping("/sign-in")
    public ResponseEntity<UserDTO> registerUser(@RequestBody UserDTO userDTO){
        return new ResponseEntity<>(userService.registerUser(userDTO), HttpStatus.OK);
    }

    @PostMapping("/login-in")
    public ResponseEntity<UserDTO> loginUser(@RequestBody UserDTO userDTO){
        return new ResponseEntity<>(userService.loginUser(userDTO), HttpStatus.OK);
    }


}
