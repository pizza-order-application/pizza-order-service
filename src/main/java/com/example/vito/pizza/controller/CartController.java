package com.example.vito.pizza.controller;

import com.example.vito.pizza.modal.dto.CartDTO;
import com.example.vito.pizza.modal.dto.OrderDTO;
import com.example.vito.pizza.modal.entity.Cart;
import com.example.vito.pizza.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    CartService cartService;

    @GetMapping("/get/{userId}")
    public ResponseEntity<CartDTO> getCartDetailsByUserId(@PathVariable("userId") Long userId){
        CartDTO cartDTO = cartService.getCartDetailsByUserId(userId);
        if(cartDTO == null){
            return new ResponseEntity<>(cartDTO, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(cartDTO, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<CartDTO> addCartDetails(@RequestBody CartDTO cartDto){
        return new ResponseEntity<>(cartService.addCartDetails(cartDto), HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<CartDTO> updateCartDetails(@RequestBody CartDTO cartDto){
        return new ResponseEntity<>(cartService.updateCartDetails(cartDto), HttpStatus.OK);
    }

}
