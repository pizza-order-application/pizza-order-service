package com.example.vito.pizza.controller;

import com.example.vito.pizza.modal.dto.InvoiceDTO;
import com.example.vito.pizza.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequestMapping("/invoice")
public class InvoiceController {

    @Autowired
    InvoiceService invoiceService;

    @GetMapping("/get/{invoiceNo}")
    public ResponseEntity<InvoiceDTO> getInvoiceByInvoiceNo(@PathVariable("invoiceNo") String invoiceNo){
        InvoiceDTO invoiceDTO = invoiceService.getInvoiceByInvoiceNo(invoiceNo);
        if(invoiceDTO == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(invoiceDTO, HttpStatus.OK);
    }

    @PostMapping("/print")
    public ResponseEntity<InvoiceDTO> printInvoice(@RequestBody InvoiceDTO invoiceDTO){
        return new ResponseEntity<>(invoiceService.printInvoice(invoiceDTO), HttpStatus.OK);
    }
}
