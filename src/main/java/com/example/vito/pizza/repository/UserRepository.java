package com.example.vito.pizza.repository;

import com.example.vito.pizza.modal.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long>, JpaSpecificationExecutor<User> {

    User findByUserIdAndIsDeleted(Long userId, boolean isDeleted);
    User findByUserIdAndIsDeletedAndIsActiveSession(Long userId, boolean isDeleted, boolean isActiveSession);
    User findByUserIdAndIsDeletedAndIsActiveSessionAndPassword(Long userId, boolean isDeleted, boolean isActiveSession, String password);

}
