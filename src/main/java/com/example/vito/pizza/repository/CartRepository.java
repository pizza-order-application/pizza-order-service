package com.example.vito.pizza.repository;

import com.example.vito.pizza.modal.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart,Long>, JpaSpecificationExecutor<Cart> {

    Cart findByUserIdAndIsDeleted(Long userId, boolean isDeleted);
}
