package com.example.vito.pizza.repository;

import com.example.vito.pizza.modal.entity.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice,Long>, JpaSpecificationExecutor<Invoice> {
    Invoice findByInvoiceNo(String invoiceNo);
}

